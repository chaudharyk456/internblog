﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internBlog.Models
{
    public class Comment
    {
        public int cid { get; set; }

        public int postId { get; set; }
        public string cuser { get; set; }   
        public string cbody { get; set; }
        public DateTime cpostDate { get; set; }
    }
}