﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internBlog.Models
{
    public class PostDetails
    {
        public int pid { get; set; }
        public string ptitle { get; set; }
        public string pbody { get; set; }
        public string pauthor { get; set; }
        public string pimagePath { get; set; }
        public DateTime ppostDate { get; set; }
        public DateTime pupdatedDate { get; set; }
        public int cid { get; set; }
        public string cuser { get; set; }
        public string cbody { get; set; }
        public DateTime cpostDate { get; set; }

    }
}