﻿using Dapper;
using internBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internBlog.Repository
{
    public class PostRepository
    {
        DBConnection dbConnection = new DBConnection();

        public List<Post> GetAllPosts()
        {
            try {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "getAll");
                var postList = SqlMapper.Query<Post>(dbConnection.conn, "sp_postData", parameters, commandType: System.Data.CommandType.StoredProcedure).ToList();
                return postList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public List<PostDetails> PostDetail(int id)
        {
            try
            {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "getById");
                parameters.Add("@id", id);
                var postDetail = SqlMapper.Query<PostDetails>(dbConnection.conn, "sp_postData", parameters, commandType: System.Data.CommandType.StoredProcedure).ToList();
                return postDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void InsertPost(Post p)
        {
            try {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "insert");
                parameters.Add("@ptitle", p.ptitle);
                parameters.Add("@pbody", p.pbody);
                parameters.Add("@pauthor", p.pauthor);
                parameters.Add("@pimagePath", p.pimagePath);
                SqlMapper.Query<Post>(dbConnection.conn, "sp_postData", parameters, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void UpdatePost(Post post)
        {
            try
            {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "update");
                parameters.Add("@id", post.pid);
                parameters.Add("@ptitle", post.ptitle);
                parameters.Add("@pbody", post.pbody);
                parameters.Add("@pimagePath", post.pimagePath);
                SqlMapper.Query<Post>(dbConnection.conn, "sp_postData", parameters, commandType: System.Data.CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void DeletePost(Post post)
        {
            try
            {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "delete");
                parameters.Add("@id", post.pid);
                SqlMapper.Query<Post>(dbConnection.conn, "sp_postData", parameters, commandType: System.Data.CommandType.StoredProcedure);
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void InsertComment(PostDetails postDetails)
        {
            try {
                dbConnection.Connection();
                dbConnection.conn.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@flag", "insert");
                parameters.Add("@postId", postDetails.pid);
                parameters.Add("@cuser", postDetails.cuser);
                parameters.Add("@cbody", postDetails.cbody);
                SqlMapper.Query<Post>(dbConnection.conn, "sp_commentData", parameters, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }
    }
}