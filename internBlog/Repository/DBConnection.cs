﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace internBlog.Repository
{
    public class DBConnection
    {
        public string connectionString = "Data Source=LAPTOP-KIRAN-LE\\CAMELCASE;Initial Catalog=Blog;Integrated Security=True";
        public SqlConnection conn;

        public void Connection()
        {
            conn = new SqlConnection(connectionString);
        }

        public void Dispose()
        {
            conn.Dispose();
        }

    }
}