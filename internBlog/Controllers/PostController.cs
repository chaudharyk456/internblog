﻿using internBlog.Models;
using internBlog.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace internBlog.Controllers
{
    public class PostController : Controller
    {
        PostRepository postRepository = new PostRepository();
        // GET: Post
        public ActionResult Index()
        {
            //var listPost = postRepository.GetAllPosts();
            return View();
        }

        public ActionResult TestView()
        {
            return View();
        }

        public JsonResult GetList()
        {
            var listPost = postRepository.GetAllPosts().Select(s => new
            {
                pid = s.pid,
                ptitle = s.ptitle,
                pbody = s.pbody,
                pauthor = s.pauthor,
                pimagePath = s.pimagePath,
                ppostDate = s.ppostDate.ToShortDateString(),
            }).ToList();
            return Json(listPost, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult InsertPost()
        { 
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult InsertPost(HttpPostedFileBase ImageFile, Post p)
        {
            string fileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
            string fileExtension = Path.GetExtension(ImageFile.FileName);
            fileName = fileName + fileExtension;
            p.pimagePath = "/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            ImageFile.SaveAs(fileName);
            postRepository.InsertPost(p);
            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePost(Post p)
        {
            postRepository.DeletePost(p);
            return Json(JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult PostDetail(int id)
        {
            var postDetails = postRepository.PostDetail(id);
            return Json(postDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PostDetail(PostDetails postDetails)
        {
            postRepository.InsertComment(postDetails);
            return Redirect("./" + postDetails.pid);
        }

        [ValidateInput(false)]
        public JsonResult UpdatePost(HttpPostedFileBase ImageFile, Post p)
        {
            if(ImageFile != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                string extension = Path.GetExtension(ImageFile.FileName);
                fileName  += extension;
                p.pimagePath = "/Images/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
                ImageFile.SaveAs(fileName);
            }
            postRepository.UpdatePost(p);
            return Json(JsonRequestBehavior.AllowGet);
        }

    }
}